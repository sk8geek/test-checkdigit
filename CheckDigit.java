public class CheckDigit {

	public static void main(String[] args) {
		CheckDigit check = new CheckDigit(args[0]);
	}
	
	/**
	 * Calculate a checkdigit using the method:
	 *
	 * 1) Digits in odd positions are summed and multiplied by three
	 * 2) Digits in even positions are summed and added to the above
	 * 3) The above is divided by ten and the remainder is taken from ten
	 * This is the checkdigit
	 *
	 */
	CheckDigit(String input) {
		int oddSum = 0;
		int evenSum = 0;
		try {
			for(int i = input.length() - 1; i > -1; i = i - 2) {
				oddSum += Integer.parseInt(input.substring(i, i + 1));
				if (i > 1) {
					evenSum += Integer.parseInt(input.substring(i - 1, i));
				}
			}
			oddSum = oddSum * 3 + evenSum;
			oddSum = oddSum % 10;
		} catch (NumberFormatException nfex) {
			// wrong
		}
		oddSum = oddSum - 10;
		if (oddSum == 10) {
			System.out.println("X");
		} else {
			System.out.println(oddSum);
		}
	}

}
