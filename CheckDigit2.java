public class CheckDigit2 {

	public static void main(String[] args) {
		CheckDigit2 check = new CheckDigit2(args[0]);
	}
	
	/**
	 * Calculate a checkdigit using the method:
	 *
	 * 1) Digits in odd positions are summed and multiplied by three
	 * 2) Digits in even positions are summed and added to the above
	 * 3) The above is divided by ten and the remainder is taken from ten
	 * This is the checkdigit
	 *
	 * This method expects a mixed alphanumeric string of the following 
	 * pattern:
	 *
	 * 1 initial capital letter
	 * 3 numbers
	 * 2 characters that are a hexadecimal pair
	 * 5 numbers 
	 *
	 */
	CheckDigit2(String input) {
		StringBuilder h = new StringBuilder(input);
		h.replace(0, 1, Integer.toString((int)h.charAt(0)));
		h.replace(5, 7, Integer.toString(Integer.parseInt(h.substring(5, 7), 16)));
		int oddSum = 0;
		int evenSum = 0;
		try {
			for(int i = h.length() - 1; i > -1; i = i - 2) {
				oddSum += Integer.parseInt(h.substring(i, i + 1));
				if (i > 1) {
					evenSum += Integer.parseInt(h.substring(i - 1, i));
				}
			}
			oddSum = oddSum * 3 + evenSum;
			oddSum = oddSum % 10;
		} catch (NumberFormatException nfex) {
			// wrong
		}
		oddSum = oddSum - 10;
		if (oddSum == 10) {
			System.out.println("X");
		} else {
			System.out.println(oddSum);
		}
	}

}
